<?php

namespace App\Models;


class Student extends BaseModel
{
    protected $table = 'students';

    protected $primaryKey = 'id';
    protected $fillable =  [
        'id','name','class_id','dob','city','created_at','updated_at'
    ];

    public function get_class(){
        return $this->belongsTo(StuClass::class,'class_id');
    }

}
